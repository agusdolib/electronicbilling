const fs = require('fs');

var dummy = require("./Dummy.js");
var createTicket = require("./CreateTicket.js");
var schedule = require('node-schedule');

var token = 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/Pgo8c3NvIHZlcnNpb249IjIuMCI+CiAgICA8aWQgc3JjPSJDTj13c2FhaG9tbywgTz1BRklQLCBDPUFSLCBTRVJJQUxOVU1CRVI9Q1VJVCAzMzY5MzQ1MDIzOSIgZHN0PSJDTj13c2ZlLCBPPUFGSVAsIEM9QVIiIHVuaXF1ZV9pZD0iMTEzMjQxMzY5MyIgZ2VuX3RpbWU9IjE1NzM0ODM2OTEiIGV4cF90aW1lPSIxNTczNTI2OTUxIi8+CiAgICA8b3BlcmF0aW9uIHR5cGU9ImxvZ2luIiB2YWx1ZT0iZ3JhbnRlZCI+CiAgICAgICAgPGxvZ2luIGVudGl0eT0iMzM2OTM0NTAyMzkiIHNlcnZpY2U9IndzZmUiIHVpZD0iU0VSSUFMTlVNQkVSPUNVSVQgMjc0MzUyMzk0OTMsIENOPXBydWViYWRvbGliIiBhdXRobWV0aG9kPSJjbXMiIHJlZ21ldGhvZD0iMjIiPgogICAgICAgICAgICA8cmVsYXRpb25zPgogICAgICAgICAgICAgICAgPHJlbGF0aW9uIGtleT0iMjc0MzUyMzk0OTMiIHJlbHR5cGU9IjQiLz4KICAgICAgICAgICAgPC9yZWxhdGlvbnM+CiAgICAgICAgPC9sb2dpbj4KICAgIDwvb3BlcmF0aW9uPgo8L3Nzbz4K';
var sign = 'N6KQqyn1s7i2dxq6iREbB3VXlokMAqf9aWVPy9LMqJUwRetTY/NQk6V+tzLhT0I7vOxH8wujOoSG6w1pXkrkOGtAg6c0vtA3sywl4kWre4sw9ZV4gA6WaJPyiusDj+v5cjJIv476igNjZ+0dSaHelBQpqaQeWdarG+IBf2r30FA=';
 

var j = schedule.scheduleJob('59 */2 * * *', function(){  // At every hour at 16 minutes. 16:16,17:16....
    createAccessTicket();
})
var i = schedule.scheduleJob('* * * * *', function(){  // At every two minutes
    createNewVoucher(token,sign);   
})

function createAccessTicket(){
    var dummyResult = dummy.wsStatus();
    if(dummyResult === 0){
        var newTicket = createTicket.ticket();
        console.log(newTicket);
        if(newTicket===0)
        {
            console.log("intentar de nuevo")
            createAccessTicket();
        }
        else{
            splitString = newTicket.split("@");
            token = splitString[0];
            sign = splitString[1];
            console.log(token + 'sign=' + sign);

        }
    }
    else {
        console.log("El webService de AFIP no esta funcionando"); //En caso de no funcionar el ws, deberiamos guardar todas 
    }                                                             //las transacciones que no pudimos generar la factura
}                                                                 //para que cuando funcione de nuevo empecemos a aprobarlas.

function createNewVoucher(token,sign){
    var newVoucher = require("./ElectronicBilling.js");
    let NewVoucher = newVoucher.NewVoucher;
    console.log('Resultado de la creacion de la factura : ');
    lastVoucher =  NewVoucher(token,sign,getLastAuthVoucher(token,sign),'20382816294',100,'PES',12,6,5);
}

function getLastAuthVoucher(token,sign){
    var lastVouchAuth = require("./LastAuthVoucher.js");
    let LastVouchAuth = lastVouchAuth.LastVouchAuth;   
    lastVoucher =  LastVouchAuth(token,sign);
    return lastVoucher;
}


