//Prueba de envio de mensaje SOAP para verificar el estado del WebService de AFIP.
var DOMParser = require('xmldom').DOMParser;
const fs = require('fs');
var createPDF = require("./CreatePDF.js");

var importeIva,importeNeto,importeTotal;
var last,_id,_importe,_tipoMoneda,_ptoVenta,_cbteTipo;
module.exports = {
    NewVoucher: NewVoucher
}
function NewVoucher(token,sign,lastVoucher,id,importe,tipoMoneda,ptoVenta,cbteTipo,iva){
    var i = 0;
    var porcentaje;
    _id = id;
    _importe = importe;
    _tipoMoneda = tipoMoneda;
    _ptoVenta = ptoVenta;
    _cbteTipo = cbteTipo;

    last = lastVoucher++;
    date = currentDate();       
    importeNeto = importe;
    switch (iva) {
        case 4:
          porcentaje = 1.10; // 10% iva
          break;
        case 5:
          porcentaje = 1.21; // 21% iva
          break;
    }
    if (iva === 0) { //Excento de impuestos.
        importeIva = 0;
    }
    else{
        importeIva = importe - (importe/porcentaje);
    }
    importeTotal = importe + importeIva;

    var str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ar="http://ar.gov.afip.dif.FEV1/">'+
         '<soapenv:Header/>'+
         '<soapenv:Body>'+
         '<ar:FECAESolicitar>'+
             '<ar:Auth>'+
			 '<ar:Token>' + token.toString() + '</ar:Token>'+
			 '<ar:Sign>' + sign.toString() + '</ar:Sign>'+
			 '<ar:Cuit>27435239493</ar:Cuit>'+
             '</ar:Auth>'+
                 '<ar:FeCAEReq>'+
                 '<ar:FeCabReq>'+
                     '<ar:CantReg>1</ar:CantReg>'+
                     '<ar:PtoVta>' + ptoVenta + '</ar:PtoVta>'+
                     '<ar:CbteTipo>' + cbteTipo + '</ar:CbteTipo>'+ 
                 '</ar:FeCabReq>'+
                '<ar:FeDetReq>'+
                     '<ar:FECAEDetRequest>'+
                             '<ar:Concepto>1</ar:Concepto>'+ 
                             '<ar:DocTipo>80</ar:DocTipo>'+ 
                             '<ar:DocNro>' + id + '</ar:DocNro>'+
                             '<ar:CbteDesde>' + lastVoucher + '</ar:CbteDesde>'+
                             '<ar:CbteHasta>' + lastVoucher + '</ar:CbteHasta>'+
                             '<ar:CbteFch>' + date + '</ar:CbteFch>'+
                             '<ImpTotal>'+ importeTotal +'</ImpTotal>'+
                             '<ImpTotConc>0</ImpTotConc>'+
                             '<ImpNeto>'+ importeNeto +'</ImpNeto>'+
                             '<ImpOpEx>0</ImpOpEx>'+
                             '<ImpTrib>0</ImpTrib>'+
                             '<ImpIVA>'+ importeIva +'</ImpIVA>'+
                             '<FchServDesde></FchServDesde>'+
                             '<FchServHasta></FchServHasta>'+
                             '<FchVtoPago></FchVtoPago>'+
                             '<ar:MonId>PES</ar:MonId>'+
                             '<ar:MonCotiz>1</ar:MonCotiz>'+
                                '<Iva>'+
                                '<AlicIva>'+
                                    '<Id>'+ iva +'</Id>'+ // 4 = 21.5 % iva; 5 = 10.5 %
                                    '<BaseImp>'+ importeTotal +'</BaseImp>'+
                                    '<Importe>'+ importeIva +'</Importe>'+
                                '</AlicIva>'+
                                '</Iva>'+
                    '</ar:FECAEDetRequest>'+
                 '</ar:FeDetReq>'+
             '</ar:FeCAEReq>'+
         '</ar:FECAESolicitar>'+
      '</soapenv:Body>'+
    '</soapenv:Envelope>'
          function createCORSRequest(method, url) {
                      var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
                      var xhr = new XMLHttpRequest();
                      if ("withCredentials" in xhr) {
                          xhr.open(method, url, false);
                      } else if (typeof XDomainRequest != "undefined") {
                          alert
                          xhr = new XDomainRequest();
                          xhr.open(method, url);
                      } else {
                          console.log("CORS not supported");
                          alert("CORS not supported");
                          xhr = null;
                      }
                      return xhr;
                  }
          var xhr = createCORSRequest("POST", "https://wswhomo.afip.gov.ar/wsfev1/service.asmx?WSDL");
          if(!xhr){
           console.log("XHR issue");
           return;
          }
  
          xhr.onload = function (){
           var results = xhr.responseText;
           isOk(results);
        }
  
          xhr.setRequestHeader('Content-Type', 'text/xml');
          xhr.send(str);
}

function isOk(results)
{
    var doc = new DOMParser().parseFromString(results, 'text/xml');
    try {
        var valueVouchNumber = doc.getElementsByTagName('CAE');
        var tempsVouchNumber = valueVouchNumber[0].firstChild.nodeValue;  
        var valueResult = doc.getElementsByTagName('Resultado');
        var tempsResult = valueResult[0].firstChild.nodeValue;
        var valueId = doc.getElementsByTagName('DocNro');
        var tempsId = valueId[0].firstChild.nodeValue;
        if(tempsResult.toString() === 'A' ){  
            console.log('La factura ha sido aprobada de manera correcta el numero de comprobante es: ' + tempsVouchNumber.toString());
            console.log('El dni del cliente es: ' + tempsId.toString());
            let CreatePDF = createPDF.CreatePDF;
            console.log('Resultado de la creacion del pdf y envio: ');
            var str = "ut";
            lastVoucher =  CreatePDF(tempsVouchNumber.toString(),importeNeto.toFixed(2),importeIva,importeTotal);
        
        }
    }
    catch(err){
            console.log("Error al crear el voucher");
    }
}

function currentDate(){
    // Return today's date and time
    var currentTime = new Date();

    // returns the month (from 0 to 11)
    var month = currentTime.getMonth() + 1;

    // returns the day of the month (from 1 to 31)
    var day = currentTime.getDate();

    // returns the year (four digits)
    var year = currentTime.getFullYear();

    // write output yyyymmdd
    return (year + "" + month + "" + day);
}