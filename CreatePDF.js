var request = require('request');
var fs = require('fs');
var sendEmail = require("./SendEmail.js");

module.exports = {
    CreatePDF: CreatePDF
}

var data = {
    nombre : "Agustin",
    mail : "aguslop0921@gmail.com",
    id : 20382816294,
    importe : 100,
    moneda : "PESO",
    ptoVenta : 12,
    cbteTipo : 6,
    nroCte : 2314556,
    formaPago : 'MERCADO PAGO',
    ultimosDigitos : 1007
}

// CreatePDF(nombre,mail.id,importe,moneda,ptoVenta,cbteTipo,nroCte,formaPago,ultimosDigitos);
function CreatePDF(nroCbte,importeNeto,importeIva,importeTotal){
    var content = '<!doctype html>'+
    '<html>'+
    '<head>'+
        '<meta charset="utf-8">'+
        '<style>'+
        '.invoice-box {'+
         'max-width: 800px;'+
            'margin: auto;'+
            'padding: 30px;'+
            'border: 1px solid #eee;'+
            'box-shadow: 0 0 10px rgba(0, 0, 0, .15);'+
            'font-size: 16px;'+
            'line-height: 24px;'+
            'color: #555;'+
        '}'+
        
        '.invoice-box table {'+
            'width: 100%;'+
            'line-height: inherit;'+
            'text-align: left;'+
        '}'+
        
        '.invoice-box table td {'+
            'padding: 5px;'+
            'vertical-align: top;'+
        '}'+
        
        '.invoice-box table tr td:nth-child(2) {'+
            'text-align: right;'+
        '}'+
        
        '.invoice-box table tr.top table td {'+
            'padding-bottom: 20px;'+
        '}'+
        
        '.invoice-box table tr.top table td.title {'+
            'font-size: 45px;'+
            'line-height: 45px;'+
            'color: #333;'+
        '}'+
        
        '.invoice-box table tr.information table td {'+
            'padding-bottom: 40px;'+
        '}'+
        
        '.invoice-box table tr.heading td {'+
            'background: #eee;'+
            'border-bottom: 1px solid #ddd;'+
            'font-weight: bold;'+
        '}'+
        
        '.invoice-box table tr.details td {'+
            'padding-bottom: 20px;'+
        '}'+
        
        '.invoice-box table tr.item td{'+
            'border-bottom: 1px solid #eee;'+
        '}'+
        
        '.invoice-box table tr.item.last td {'+
            'border-bottom: none;'+
        '}'+
        
        '.invoice-box table tr.total td:nth-child(2) {'+
            'border-top: 2px solid #eee;'+
            'font-weight: bold;'+
        '}'+
        
        '@media only screen and (max-width: 600px) {'+
            '.invoice-box table tr.top table td {'+
                'width: 100%;'+
                'display: block;'+
                'text-align: center;'+
            '}'+
            
            '.invoice-box table tr.information table td {'+
                'width: 100%;'+
                'display: block;'+
                'text-align: center;'+
            '}'+
        '}'+
        
        '/** RTL **/'+
        '.rtl {'+
            'direction: rtl;'+
        '}'+
        
        '.rtl table {'+
            'text-align: right;'+
        '}'+
        
        '.rtl table tr td:nth-child(2) {'+
            'text-align: left;'+
        '}'+
        '</style>'+
    '</head>'+
    
    '<body>'+
        '<div class="invoice-box">'+
            '<table cellpadding="0" cellspacing="0">'+
                '<tr class="top">'+
                    '<td colspan="2">'+
                        '<table>'+
                            '<tr>'+
                                '<td class="title">'+
                                    '<img src="https://dolareslibre.com/images/logo-dolareslibre.jpeg" style="width:100%; max-width:100px;">'+
                                '</td>'+
                                
                                '<td>'+
                                    'Numero de comprobante #: '+ data.nroCte +'<br>'+
                                    'Fecha: '+ currentDate() +'<br>'+
                                '</td>'+
                            '</tr>'+
                        '</table>'+
                    '</td>'+
                '</tr>'+
                
                '<tr class="information">'+
                    '<td colspan="2">'+
                        '<table>'+
                            '<tr>'+
                                '<td>'+
                                    'Dolares Libres,<br>'+
                                    'Bv San Juan 639<br>'+
                                    'Cordoba, 5001<br>'+
                                '</td>'+
                                
                                '<td>'+
                                    ' '+data.nombre+'<br>'+
                                    ' '+data.mail+''+
                                '</td>'+
                            '</tr>'+
                        '</table>'+
                    '</td>'+
                '</tr>'+
                
                '<tr class="heading">'+
                    '<td>'+
                        'Metodo de pago'+
                    '</td>'+
                    
                    '<td>'+
                        'Ultimos digitos #'+
                    '</td>'+
                '</tr>'+
                
                '<tr class="details">'+
                    '<td>'+
                        'Tarjeta de credito'+
                    '</td>'+
                    
                    '<td>'+
                        ' '+ data.importe +''+
                    '</td>'+
                '</tr>'+
                
                '<tr class="heading">'+
                    '<td>'+
                        'Item'+
                    '</td>'+
                    
                    '<td>'+
                        'Precio'+
                    '</td>'+
                '</tr>'+
                
                '<tr class="item">'+
                    '<td>'+
                        'Compra Dolares'+
                    '</td>'+
                    
                    '<td>'+
                        '$'+ importeNeto +' '+
                    '</td>'+
                '</tr>'+
                '<tr class="item">'+
                    '<td>'+
                        'IVA'+
                    '</td>'+
                    
                    '<td>'+
                        '$'+ importeIva.toFixed(2) +' '+
                    '</td>'+
                '</tr>'+
                
                // '<tr class="item">'+
                //     '<td>'+
                //         'Hosting (3 months)'+
                //     '</td>'+
                    
                //     '<td>'+
                //         '$75.00'+
                //     '</td>'+
                // '</tr>'+
                
                // '<tr class="item last">'+
                //     '<td>'+
                //         'Domain name (1 year)'+
                //     '</td>'+
                    
                //     '<td>'+
                //         '$10.00'+
                //     '</td>'+
                // '</tr>'+
                
                '<tr class="total">'+
                    '<td></td>'+
                    
                    '<td>'+
                       'Total: $'+importeTotal.toFixed(2)+''+
                    '</td>'+
                '</tr>'+
            '</table>'+
        '</div>'+
    '</body>'+
    '</html>'

config = {
  url: 'https://docraptor.com/docs',
  encoding: null, //IMPORTANT! This produces a binary body response instead of text
  headers: {
    'Content-Type': 'application/json'
  },
  json: {
    user_credentials: "YOUR_API_KEY_HERE",
    doc: {
      document_content: content,
      type: "pdf",
      test: true,
      prince_options: {
        media:   "screen",          // use screen styles instead of print styles
        baseurl: "http://hello.com" // URL to use for generating absolute URLs for assets from relative URLs
      }
     }
    }
  }

request.post(config, function(err, response, body) {
        request.post(config, function(err, response, body) {
            // var nombrePDF = data.nombre+"-"+nroCbte +'.pdf';
            var nombrePDF = data.nombre+'.pdf';

            fs.writeFile(nombrePDF, body, "binary", function(writeErr) {
            console.log('PDF GUARDADO!');
            // sendEmail.SendEmail('aguslop0921@gmail.com',nombrePDF);
            });
        });
    });
}

function currentDate(){
    // Return today's date and time
    var currentTime = new Date();

    // returns the month (from 0 to 11)
    var month = currentTime.getMonth() + 1;

    // returns the day of the month (from 1 to 31)
    var day = currentTime.getDate();

    // returns the year (four digits)
    var year = currentTime.getFullYear();

    // write output yyyymmdd
    return (month + "/" + day + "/" + year);
}