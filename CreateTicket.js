//Generacion del ticketRequest para generar certificados.
var DOMParser = require('xmldom').DOMParser;
const readline = require('readline');
const xml2js = require('xml2js');
const fs = require('fs');

var text = '';
var ticket;

module.exports = { 
    ticket : function()
    {
        let date = new Date();
        var builder = require('xmlbuilder');
        var doc = builder.create('loginTicketRequest')
            .ele('header')
                .ele('uniqueId')
                .txt(Math.floor(date.getTime() / 1000)) 
                .up()
                .ele('generationTime')
                .txt(new Date(date.getTime() - 10800000).toISOString())
                .up()
                .ele('expirationTime')
                .txt(new Date(date.getTime() + 10200000).toISOString())
                .up()
            .up()
        .ele('service')
            .txt('wsfe')
        .end({ pretty: true });
        fs.writeFileSync('./files/MiLoginTicket.xml',doc,{encoding:'utf8',flag:'w'});
        this.getSignedCertificate();
        return this.readTA();
    },	

    //Funcion para ejecutar en la consola openssl con el certificado y la clave privada para obtener el TA
    getSignedCertificate : function()
    {
        const exec = require('child_process').exec;
        const child = exec('openssl smime -sign -in ./files/MiLoginTicket.xml -out ./files/MiLoginTicket.xml.cms -signer ./files/certificado.pem -inkey ./files/privada.key -nodetach -outform PEM',
            (error, stdout, stderr) => {
                if (error !== null) {
                    console.log(`exec error: ${error}`);
                }
        });

    },

    //Funcion para leer el certificado generado con openssl, despreciando la primera y ultima linea que son
    //el inicio y fin del certificado. La mayoria de pruebas que hice, el certificado tiene exactamente 
    //40 lineas, pero para evitar errores cada vez que se genere un nuevo certificado, cuento las lineas.
    readTA : function()
    {
        var i = 0;
        filePath = './files/MiLoginTicket.xml.cms';

        //Count lines
        fileBuffer =  fs.readFileSync(filePath);
        to_string = fileBuffer.toString();
        split_lines = to_string.split("\n");

        //readlines skipping the first and the last one.
        readline.createInterface({
            input: fs.createReadStream(filePath),
            terminal: false
        }).on('line', function(line) {
            if( i !== 0 && i < split_lines.length - 2 )
            {
                    fs.appendFileSync('./files/MiLoginTicket.txt', line );
            }
            i++;
        });
        return this.soapRequest();
    },

    //Soap request, con el certificado ya generado, lo que retorne esta funcion voy a tener que analizarlo para
    //poder extraer el token y el sign
    soapRequest : function(){

        var loginTicket =  fs.readFileSync('./files/MiLoginTicket.txt');
        var str =   '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsaa="http://wsaa.view.sua.dvadac.desein.afip.gov">'+
                        '<soapenv:Header/>'+
                             '<soapenv:Body>'+
                                '<wsaa:loginCms>'+
                                     '<wsaa:in0>'+ loginTicket +'</wsaa:in0>'+
                                '</wsaa:loginCms>'+
                            '</soapenv:Body>'+
                    '</soapenv:Envelope>'
            function createCORSRequest(method, url) {
                        var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
                        var xhr = new XMLHttpRequest();
                        if ("withCredentials" in xhr) {
                            xhr.open(method, url, false);
                        } else if (typeof XDomainRequest != "undefined") {
                            alert
                            xhr = new XDomainRequest();
                            xhr.open(method, url);
                        } else {
                            console.log("CORS not supported");
                            alert("CORS not supported");
                            xhr = null;
                        }
                        return xhr;
                    }
            var xhr = createCORSRequest("POST", "https://wsaahomo.afip.gov.ar/ws/services/LoginCms?WSDL");
            if(!xhr){
            console.log("XHR issue");
            return;
            }
            fs.writeFileSync('./files/MiLoginTicket.txt','',{encoding:'utf8',flag:'w'});
            xhr.onload = function (){
                let results = xhr.responseText;
                var doc = new DOMParser().parseFromString(unescapeHTML(results), 'text/xml');

                try { 
                    var valueToken = doc.getElementsByTagName('token');
                    var token = valueToken[0].firstChild.nodeValue;  

                    var valueSign = doc.getElementsByTagName('sign');
                    var sign = valueSign[0].firstChild.nodeValue;
                    console.log(token);
                    console.log(sign);
                    ticket = token+'@'+sign;
                    fs.writeFileSync('./files/access.txt',ticket,{encoding:'utf8',flag:'w'});
                    return doc;
                }
                catch(err){
                    return 0;
                }
            }
            xhr.setRequestHeader('text/xml;charset=utf-16')
            xhr.setRequestHeader('soapAction','https://wsaahomo.afip.gov.ar/ws/services/LoginCms');
            xhr.send(str);
            return ticket;
    },
}    
function unescapeHTML(escapedHTML) {
    return escapedHTML.replace(/&lt;/g,'<').replace(/&gt;/g,'>').replace(/&amp;/g,'&').replace(/&quot;/g,'"');
}
